create table movie (movie_id integer primary key auto_increment, movie_title varchar(20), movie_release_date varchar(20), movie_time integer, director_name varchar(20));
insert into movie (movie_title,movie_release_date,movie_time,director_name) values('movie1','10/11/2020',3,'mahesh');
insert into movie (movie_title,movie_release_date,movie_time,director_name) values('movie2','11/11/2020',3,'nilesh');
insert into movie (movie_title,movie_release_date,movie_time,director_name) values('movie3','12/11/2020',3,'sarvesh');