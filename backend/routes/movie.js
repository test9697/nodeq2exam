const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

router.get("/displaymovie/:name", (request, response) => {
  console.log("here");
  const { name } = request.params;
  const query =
    "select  movie_id,movie_title,movie_release_date,movie_time,director_name where  from movie where movie_title=?";
  db.pool.execute(query, [name], (error, persons) => {
    response.send(utils.createResult(error, persons));
  });
});

router.post("/addmovie", (request, response) => {
  const { movie_title, movie_release_date, movie_time, director_name } =
    request.body;
  const query = `insert into movie (movie_title,movie_release_date,movie_time,director_name) values (?,?,?,?)`;
  db.pool.execute(
    query,
    [movie_title, movie_release_date, movie_time, director_name],
    (error, persons) => {
      response.send(utils.createResult(error, persons));
    }
  );
});

router.delete("/delete/:id", (request, response) => {
  const { id } = request.params;
  const query = `delete from movie where movie_id=?`;
  db.pool.execute(query, [id], (error, persons) => {
    response.send(utils.createResult(error, persons));
  });
});

module.exports = router;
