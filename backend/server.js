const express = require("express");
const cors = require("cors");
const routerPerson = require("./routes/movie");

const app = express();

app.use(cors("*"));
app.use(express.json());
app.use("/movie", routerPerson);

app.listen(7000, "0.0.0.0", () => {
  console.log("Server running on port 7000 successfully");
});
